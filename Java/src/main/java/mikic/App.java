package mikic;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 */
public class App {


    public static void main(String[] args) throws IOException {


/*        BlogLogController blogLogController = new BlogLogController();
        BlogLogService blogLogService = new BlogLogService();
        BlogLogMapper mapper = new BlogLogMapper();
        HtmlService htmlService = new HtmlService();

       //BlogLogDTO test = new BlogLogDTO("First headline", "My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too.", "Samuel L Jackson", new Date(System.currentTimeMillis()));
        String jsonString = blogLogController.getBlogFromAPI(2);
        blogLogService.saveToDB(jsonString);

        BlogLogDTO test = mapper.BlogToDtoWithDate(blogLogService.getFromDatabase(11));
        htmlService.writeHTML(test);*/

        Runnable r1 = new MultthreadingSaveDB();
        Runnable r2 = new MultthreadingWriteHtml();


        ExecutorService pool = Executors.newFixedThreadPool(3);

        pool.execute(r1);
        pool.execute(r2);

        pool.shutdown();
    }
}
