package mikic;

import mikic.Controller.BlogLogController;
import mikic.Services.BlogLogService;
import mikic.Services.DTOs.BlogLogDTO;
import mikic.Services.HtmlService;
import mikic.Services.Mapper.BlogLogMapper;

import java.io.IOException;

public class MultthreadingWriteHtml implements Runnable {

    private BlogLogService blogLogService;
    private BlogLogController blogLogController;
    private BlogLogMapper blogLogMapper;
    private HtmlService htmlService;


    @Override
    public void run() {

        this.blogLogMapper = new BlogLogMapper();
        this.htmlService = new HtmlService();
        this.blogLogService = new BlogLogService();


        try {


            BlogLogDTO test = blogLogMapper.BlogToDtoWithDate(blogLogService.getFromDatabase(46));
            htmlService.writeHTML(test);

            System.out.println("Thread 2: " + Thread.currentThread());
            Thread.sleep(20);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
