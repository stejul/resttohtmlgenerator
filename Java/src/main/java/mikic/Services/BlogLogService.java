package mikic.Services;

import lombok.NoArgsConstructor;
import mikic.Models.BlogLog;
import mikic.Services.DTOs.BlogLogDTO;
import mikic.Services.Mapper.BlogLogMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Responsible for the database persistency operations
 * @author Stefan Mikic
 * @version 1
 */
@NoArgsConstructor
public class BlogLogService {

    private BlogLog blogLog;
    private BlogLogDTO blogLogDTO;


    /**
     * Initializing Constructor with Entities and DTO objects
     * @param bl Entity
     * @param bld DTO object
     */
    public BlogLogService(BlogLog bl, BlogLogDTO bld) {

        this.blogLog = bl;
        this.blogLogDTO = bld;
    }


    /**
     * Converts the JSON string into a Entity object and saves it to the database
     * @param jsonString JSON string
     */
    public void saveToDB(String jsonString) {

        BlogLogMapper mapper = new BlogLogMapper();
        Calendar cal = new GregorianCalendar();
        BlogLogDTO blogLogDTO = mapper.JsonToDTO(jsonString);

        BlogLog blogLog = mapper.DTOtoBlog(blogLogDTO);
        blogLog.setLogAt(cal.getTime());

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        session.save(blogLog);
        session.getTransaction().commit();
        session.close();

    }

    /**
     * Searches the database of entities by ID and returns the entity object
     * @param id Blog post ID
     * @return BlogLog entity
     */
    public BlogLog getFromDatabase(Integer id) {


        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        BlogLog blogLog;

        session.beginTransaction();

        blogLog = session.get(BlogLog.class, id);

        session.getTransaction().commit();
        System.out.println(blogLog);
        session.close();
        return blogLog;


    }
}
