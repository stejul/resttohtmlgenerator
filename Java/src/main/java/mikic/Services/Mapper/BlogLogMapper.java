package mikic.Services.Mapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.NoArgsConstructor;
import mikic.Models.BlogLog;
import mikic.Services.DTOs.BlogLogDTO;

/**
 * Mapping the DTO Model to the Entity Model and vice versa -> DTO Pattern
 * @author Stefan Mikic
 * @version 1
 */
@NoArgsConstructor
public class BlogLogMapper {


    /**
     * Converts the DTO model into a Entity model
     * @param dto DTO Object
     * @return new Entity model
     */
    public BlogLog DTOtoBlog(BlogLogDTO dto) {
        return new BlogLog(dto.getHeadline(), dto.getContent(), dto.getAuthor());
    }

    /**
     * Converts Entity to DTO object
     * @param entity Entity
     * @return new DTO object
     */
    public BlogLogDTO BlogToDTO(BlogLog entity) {

        return new BlogLogDTO(entity.getHeadline(), entity.getContent(), entity.getAuthor());
    }

    /**
     * Converts entity to DTO object with date
     * @param entity Entity object
     * @return new DTO object
     */
    public BlogLogDTO BlogToDtoWithDate(BlogLog entity){

        return new BlogLogDTO(entity.getHeadline(), entity.getContent(), entity.getAuthor(), entity.getLogAt());
    }

    /**
     * Converts a JSON String into a DTO model
     * @param jsonString JSON string
     * @return new DTO Object
     */
    public BlogLogDTO JsonToDTO(String jsonString) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();

        BlogLogDTO blogLogDTO = gson.fromJson(jsonString, BlogLogDTO.class);

        return blogLogDTO;
    }


}
