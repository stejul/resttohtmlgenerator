package mikic.Services;


import lombok.NoArgsConstructor;
import mikic.Services.DTOs.BlogLogDTO;
import mikic.Services.Mapper.BlogLogMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static j2html.TagCreator.*;

/**
 * Uses the DTO object to generate static HTML blog posts
 * @author Stefan Mikic
 * @version 1
 */
@NoArgsConstructor
public class HtmlService {

    /**
     * Creates a template HTML file with the given DTO object
     * @param blogLog DTO Object containing the blog post from the origin BlogLog entity
     * @throws IOException
     */
    public void writeHTML(BlogLogDTO blogLog) throws IOException {

        BlogLogMapper mapper = new BlogLogMapper();

       String htmlOutput = html(
        head(
                link().withRel("stylesheet").withHref("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\template\\vendor\\bootstrap\\css\\bootstrap.min.css"),
                link().withRel("stylesheet").withType("text/css").withHref("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\template\\vendor\\fontawesome-free\\css\\all.min.css"),
                link().withRel("stylesheet").withType("text/css").withHref("https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"),
                link().withRel("stylesheet").withType("text/css").withHref("https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"),
                link().withRel("stylesheet").withHref("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\template\\css\\clean-blog.min.css")
        ),
        body(
                article(
                        div().withClass("container"),
                        div().withClass("row"),
                        div().withClass("col-lg-8 col-md-10 mx-auto"),
                        h1(blogLog.getHeadline()).withId("headlineLog"),
                        p(blogLog.getContent()).withId("contentLog"),
                        p(em("Written by: " + blogLog.getAuthor()).withClass("authorLog"))
                )
            ),
                hr(),
                footer(
                        div().withClass("container"),
                        div().withClass("row"),
                        div().withClass("col-lg-8 col-md-10 mx-auto")
                ),
                script().withSrc("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\template\\vendor\\jquery\\jquery.min.js"),
                script().withSrc("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\template\\vendor\\bootstrap\\js\\bootstrap.bundle.min.js"),
                script().withSrc("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\template\\js\\clean-blog.min.js")
        ).render();

       createStaticFile(htmlOutput, "static", blogLog );



    }


    /**
     * Uses the generated html file with the corresponding content and creates a new static HTML file
     * @param filecontent Generated HTML file
     * @param filename filename
     * @param blogLogDTO DTO object to create a folder hierachy containg the static html file
     * @throws IOException
     */
    public void createStaticFile(String filecontent, String filename, BlogLogDTO blogLogDTO)throws IOException {


        String date = blogLogDTO.getLogAt().toString();

        String[] dateSplit = date.split("-");






        File rootDir = new File("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\public\\"+dateSplit[0]);
        if (!rootDir.exists()) {
            if (rootDir.mkdir()) {
                System.out.println("Directory created");
            }
        }

        File secondDir = new File("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\public\\"+dateSplit[0]+"\\"+dateSplit[1]);

        if (!secondDir.exists()){
            if (secondDir.mkdir()){
                System.out.println("Sub directory created");
            }
        }

        File thirdDir = new File("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\public\\"+dateSplit[0]+"\\"+dateSplit[1]+"\\"+dateSplit[2]);
        if (!thirdDir.exists()){
            if (thirdDir.mkdir()){
                System.out.println("sub-sub direcotry created");
            }
        } else {
            System.out.println("Couldn't create directories");
        }

        File htmlOutput = new File("D:\\Java_Workspace\\stolz_static_generator\\Java\\src\\main\\java\\resources\\public\\"+dateSplit[0]+"\\"+dateSplit[1]+"\\"+dateSplit[2]+"\\"+filename+".html");
        BufferedWriter writer = new BufferedWriter(new FileWriter(htmlOutput));
        writer.write(filecontent);
        writer.close();



    }
}
