package mikic.Services.DTOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Responsible for turning the API calls into entities -> DTO Pattern
 * @author Stefan Mikic
 * @version 1
 */
@Getter
@NoArgsConstructor
public class BlogLogDTO {

    @SerializedName("headline")
    @Expose(serialize = true)
    private String headline;

    @SerializedName("content")
    @Expose(serialize = true)
    private String content;

    @SerializedName("author")
    @Expose(serialize = true)
    private String author;

    @Expose(deserialize = true)
    private Date logAt;


    /**
     * Allowing the constructor get the JSON String, and turn it into a DTO Model for the Entity
     * @param hd Headline
     * @param cnt Content
     * @param ath Author
     */
    public BlogLogDTO(String hd, String cnt, String ath) {
        this.headline = hd;
        this.content = cnt;
        this.author = ath;
    }

    /**
     * Allowing the constructor to contain current Date for the entity Logging
     * @param hd Headline
     * @param cnt Content
     * @param ath Author
     * @param lgt loggedAt
     */
    public BlogLogDTO(String hd, String cnt, String ath, Date lgt){
        this.headline = hd;
        this.content = cnt;
        this.author = ath;
        this.logAt = lgt;
    }
}
