package mikic.Controller;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class is responsible for calling the API-Calls from the REST-Service
 * @author Stefan Mikic
 * @version 1
 */
@Data
@AllArgsConstructor
public class BlogLogController {


    /**
     * Gets the JSON String for the Blog entity
     * @param id ID for the blog post
     * @return JSON String of the blog entity
     * @exception MalformedURLException
     * @exception IOException
     */
    public String getBlogFromAPI(Integer id) {

        try {
            URL url = new URL("http://localhost:8000/api/blog/" + id);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {

                throw new RuntimeException("Failed: HTTP Error Code :" + conn.getResponseCode());

            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String jsonOutput = br.readLine();

                return jsonOutput;
            }
        } catch (MalformedURLException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();

        }

        return null;

    }
}
