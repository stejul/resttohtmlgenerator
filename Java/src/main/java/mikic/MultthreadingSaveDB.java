package mikic;

import mikic.Controller.BlogLogController;
import mikic.Services.BlogLogService;
import mikic.Services.HtmlService;
import mikic.Services.Mapper.BlogLogMapper;

public class MultthreadingSaveDB implements Runnable {

    private BlogLogService blogLogService;
    private BlogLogController blogLogController;
    private BlogLogMapper blogLogMapper;
    private HtmlService htmlService;


    @Override
    public void run() {

        this.blogLogService = new BlogLogService();
        this.blogLogController = new BlogLogController();
        this.blogLogMapper = new BlogLogMapper();
        this.htmlService = new HtmlService();


        try {
            String jsonString = blogLogController.getBlogFromAPI(1);
            blogLogService.saveToDB(jsonString);

/*            BlogLogDTO test = blogLogMapper.BlogToDtoWithDate(blogLogService.getFromDatabase(11));
            htmlService.writeHTML(test);*/
            System.out.println("Thread 1: " + Thread.currentThread());
            Thread.sleep(20);


            MultthreadingWriteHtml mt = new MultthreadingWriteHtml();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
