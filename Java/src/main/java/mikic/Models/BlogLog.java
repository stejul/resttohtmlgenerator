package mikic.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Blog entity for logging the JSON calls into the database -> Persistency
 * @author Stefan Mikic
 * @version 1
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 100000)
    private String headline;

    @Column(nullable = false, length = 100000)
    private String content;

    @Column(nullable = false, length = 100000)
    private String author;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date logAt;


    public BlogLog(String headline, String content, String author) {
        this.headline = headline;
        this.content = content;
        this.author = author;
    }
}
