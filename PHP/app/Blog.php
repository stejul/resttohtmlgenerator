<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $fillable=[
        "headline", "content", "author" ,
    ];


    protected $hidden=[
      "created_at", "updated_at"
    ];
}
